﻿using System.Web;
using System.Web.Http.Filters;

namespace EdBlog.Api.Filters
{
    public class NoResponseCookieAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            HttpContext.Current.Items.Add("remove-auth-cookie", "true");
        }
    }
}