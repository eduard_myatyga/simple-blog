﻿using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using EdBlog.Api.Controllers.Base;
using EdBlog.Data.Enum;
using EdBlog.Data.Interfaces;
using EdBlog.Models.Models;

namespace EdBlog.Api.Controllers.APIs
{
    [RoutePrefix("api/posts")]
    [Authorize(Roles = "Admin")]
    public class PostController : BaseApiController<Post>
    {
        private int pageSize = 10;

        public PostController(IService<Post> service)
            : base(service)
        {

        }
        // GET: api/Post
        [Route("all")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetAll([FromUri]int page = 1)
        {
            return PerformReturnRequest(Request,
              delegate
              {
                  return Service.UnitOfWork.Repository<Post>().GetAll(page, pageSize, x => x.Id, x => x.Published, OrderBy.Descending,
                      x => x.Category, x => x.Author, x => x.Comments, x => x.Tags, x => x.PostImages);
              },
              MethodBase.GetCurrentMethod());
        }
        [Route("{category}/{page}")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetByCategory([FromUri]string category, [FromUri]int page = 1)
        {
            return PerformReturnRequest(Request,
              delegate
              {
                  return Service.GetAll(page, pageSize, selectBy => selectBy.Id, predicate => predicate.Category.UrlSlug == category && predicate.Published, OrderBy.Descending);
              },
              MethodBase.GetCurrentMethod());
        }
        // GET: api/Post/5
        [Route("{name}")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetPostByUrlSlug([FromUri]string name)
        {
            return PerformReturnRequest(Request,
               delegate
               {
                   return Service.UnitOfWork.Repository<Post>()
                       .GetSingleBy(x => x.Published && x.UrlSlug.Contains(name)); //todo
               },
               MethodBase.GetCurrentMethod());
        }

        // POST: api/Post
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Post/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Post/5
        public void Delete(int id)
        {
        }
    }
}
