﻿using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using EdBlog.Api.Controllers.Base;
using EdBlog.Data.Enum;
using EdBlog.Data.Interfaces;
using EdBlog.Models.Models;

namespace EdBlog.Api.Controllers.APIs
{
    [RoutePrefix("api/tags")]
    [Authorize(Roles = "Admin")]
    public class TagsController : BaseApiController<Tag>
    {
        private int pageSize = 10;
        
        public TagsController(IService<Tag> service)
            : base(service)
        {
        }
        // GET api/values/5
        [Route("")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetWithPage([FromUri]int page = 1)
        {
            return PerformReturnRequest(Request,
              delegate
              {
                  return Service.GetAll(page, pageSize, x => x.Id, OrderBy.Descending);
              },
              MethodBase.GetCurrentMethod());
        }

        // POST api/values
        [HttpPost]
        [Route("create")]
        public HttpResponseMessage Post([FromBody]Tag tag)
        {
            return PerformVoidRequest(Request,
              delegate
              {
                  Service.Add(tag);
              },
              MethodBase.GetCurrentMethod());
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }      
    }
}
