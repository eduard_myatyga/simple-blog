﻿using System;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using EdBlog.Data.Identity;
using EdBlog.Data.Interfaces;
using EdBlog.Models.Models;
using EdBlog.Models.Models.Exceptions;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace EdBlog.Api.Controllers.Base
{
    public class BaseApiController<T>:ApiController where T:BaseEntity
    {
        private ApplicationUserManager _userManager;

        public delegate void ControllerAuthVoidExecutor();

        public delegate Y ControllerAuthReturnExecutor<out Y>();

        protected IService<T> Service { get; private set; }


        protected IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        protected ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public BaseApiController(IService<T> service)
        {
            Service = service;
        }
       
        public HttpResponseMessage PerformVoidRequest(HttpRequestMessage request, ControllerAuthVoidExecutor executor, MethodBase method)
        {
            
            try
            {
                executor();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (CustomException e)
            {
                var error = new ErrorDetails
                {
                    ErrorCode = (int) e.GetCode(),
                    ErrorMessage = e.Message
                };

                return Request.CreateResponse(HttpStatusCode.BadRequest, error);
            }
            catch (Exception e)
            {
                //string methodName = method.Name;
                //Logger.Warn(String.Format("{0} input parameters: ", methodName), request);
                //WebFacade.Logger.Error(String.Format("Failed to process {0}", methodName), e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,e.Message);
            }
        }

        public HttpResponseMessage PerformReturnRequest<Y>(HttpRequestMessage request, ControllerAuthReturnExecutor<Y> executor, MethodBase method)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, executor());
            }
            catch (CustomException e)
            {
                var error = new ErrorDetails
                {
                    ErrorCode = (int) e.GetCode(),
                    ErrorMessage = e.Message
                };

                return Request.CreateResponse(HttpStatusCode.BadRequest, error);
            }
            catch (Exception e)
            {
               // string methodName = method.Name;
               // WebFacade.Logger.Warn(String.Format("{0} input parameters: ", methodName), request);
              //  WebFacade.Logger.Error(String.Format("Failed to process {0}", methodName), e);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}