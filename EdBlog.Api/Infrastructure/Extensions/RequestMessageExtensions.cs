﻿using System.Net.Http;
using System.Web.Http.Dependencies;
using EdBlog.Data.Interfaces;
using EdBlog.Models.Models;

namespace EdBlog.Api.Infrastructure.Extensions
{
    public static class RequestMessageExtensions
    {

        internal static IRepository<T> GetDataRepository<T>(this HttpRequestMessage request) where T :  BaseEntity, new()
        {
            return request.GetService<IRepository<T>>();
        }

        private static TService GetService<TService>(this HttpRequestMessage request)
        {
            IDependencyScope dependencyScope = request.GetDependencyScope();
            TService service = (TService)dependencyScope.GetService(typeof(TService));

            return service;
        }
    }
}