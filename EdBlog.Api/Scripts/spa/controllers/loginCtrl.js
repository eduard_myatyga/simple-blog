﻿(function (app) {
    'use strict';

    app.controller('loginCtrl', loginCtrl);

    loginCtrl.$inject = ['$scope', 'membershipService', 'notificationService', '$rootScope', '$location'];

    function loginCtrl($scope, membershipService, notificationService, $rootScope, $location) {

        $scope.pageClass = 'page-login';
        $scope.login = login;
        $scope.user = {};

        function login() {
            if ($scope.userLoginForm.$invalid || $scope.userLoginForm.inputUsername.$invalid)
            {
                notificationService.displayError('Login format is incorrect.');
                return;
            }
            if ($scope.userLoginForm.$invalid || $scope.userLoginForm.inputPassword.$invalid) {
                notificationService.displayError('Password format is incorrect.');
                return;
            }
            membershipService.login($scope.user, loginCompleted, loginFailed);
        }
   
        function loginCompleted(result) {
            if (result.status == 200) {
                var dataUser = { token: result.data.access_token, userName: $scope.user.username };
                membershipService.saveCredentials(dataUser);
                notificationService.displaySuccess('Hello ' + $scope.user.username);
                $scope.userData.displayUserInfo();
                if ($rootScope.previousState)
                    $location.path($rootScope.previousState);
                else
                    $location.path('/');
            }
        }

        function loginFailed(result) {
            notificationService.displayError('Login failed. Try again.');
        }
    }

})(angular.module('common.core'));