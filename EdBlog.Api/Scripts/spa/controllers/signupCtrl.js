﻿(function (app) {
    'use strict';

    app.controller('signupCtrl', signupCtrl);

    signupCtrl.$inject = ['$scope', 'membershipService', 'notificationService', '$rootScope', '$location', '$timeout'];

    function signupCtrl($scope, membershipService, notificationService, $rootScope, $location, $timeout) {
        $scope.savedSuccessfully = false;
        $scope.message = "";

        $scope.registration = {
            email: "",
            password: "",
            confirmPassword: ""
        };

        $scope.signUp = function () {
            if ($scope.userRegistrationForm.$invalid)
                return false;
            membershipService.register($scope.registration, registerCompleted);
        };
        function registerCompleted(result) {
            console.log(result);
            if (result.status == 200) {
                $scope.savedSuccessfully = true;
                notificationService.displaySuccess("User has been registered successfully, you will be redicted to login page in 2 seconds.");
                startTimer();
            }
        }
        var startTimer = function () {
            var timer = $timeout(function () {
                $timeout.cancel(timer);
                $location.path('/login');
            }, 2000);
        }

    }

})(angular.module('common.core'));