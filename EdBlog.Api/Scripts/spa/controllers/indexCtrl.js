﻿(function (app) {
    'use strict';

    app.controller('indexCtrl', indexCtrl);

    //indexCtrl.$inject = ['$scope', 'apiService', 'notificationService'];
    indexCtrl.$inject = ['$scope'];
    function indexCtrl($scope) {
        $scope.pageClass = 'page-home';
        $scope.loadingMovies = true;
        $scope.loadingGenres = true;
        $scope.isReadOnly = true;
        
    }

})(angular.module('mainApp'));