﻿(function (app) {
    'use strict';

    app.controller('postDetailsCtrl', postDetailsCtrl);

    postDetailsCtrl.$inject = ['$scope'];

    function postDetailsCtrl($scope) {
        $scope.pageClass = 'page-movies';
        $scope.post = {};
        $scope.loadingPost = true;
        $scope.loadingComments = true;

        $scope.isReadOnly = true;
       
    }
})(angular.module('mainApp'));