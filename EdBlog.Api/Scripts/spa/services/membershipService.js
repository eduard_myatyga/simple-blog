﻿(function (app) {
    'use strict';

    app.factory('membershipService', membershipService);

    membershipService.$inject = ['$q', 'apiService', 'notificationService', '$http', '$base64', '$cookieStore', '$rootScope'];

    function membershipService($q, apiService, notificationService, $http, $base64, $cookieStore, $rootScope) {

       // var serviceBase = 'http://localhost:8080/blog/';
       // $rootScope.repository = {};
        var service = {
            login: login,
            register: register,
            saveCredentials: saveCredentials,
            removeCredentials: removeCredentials,
            isUserLoggedIn: isUserLoggedIn
        }

        function login(user, completed, failed) {
            var userData = "grant_type=password&username=" + user.username + "&password=" + user.password;
            var config = {
                method: 'POST',
                url: 'http://localhost:8080/blog/token',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: userData
            }
            apiService.request(config,
            completed,
            failed);
        }

        function register(user, completed) {
            apiService.post('api/Account/Register', user,
            completed,
            registrationFailed);
        }

        function saveCredentials(user) {
            console.log(user);
            $rootScope.repository = {
                loggedUser: {
                    username: user.userName,
                    token: user.token
                }
            };

            $http.defaults.headers.common['Authorization'] = 'Bearer ' + user.token;
            $cookieStore.put('repository', $rootScope.repository);
        }

        function removeCredentials() {
            $rootScope.repository = {};
            $cookieStore.remove('repository');
            $http.defaults.headers.common.Authorization = '';
        };

        function loginFailed(response) {
            notificationService.displayError(response.data);
        }

        function registrationFailed(response) {
            var errors = [];
            for (var key in response.data.modelState) {
                for (var i = 0; i < response.data.modelState[key].length; i++) {
                    errors.push(response.data.modelState[key][i]);
                }
            }
            for (var i = 0; i < errors.length; i++) {
                notificationService.displayError(errors[i]);
            }
        }

        function isUserLoggedIn() {
            return $rootScope.repository.loggedUser != null;
        }

        return service;
    }



})(angular.module('common.core'));