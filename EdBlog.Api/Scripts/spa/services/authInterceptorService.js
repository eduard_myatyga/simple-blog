﻿(function (app) {
    'use strict';

    app.factory('authInterceptorService', authInterceptorService);

    authInterceptorService.$inject = ['$q', '$location', '$cookieStore'];

    function authInterceptorService($q, $location, $cookieStore) {

        var authInterceptorServiceFactory = {};

        var _request = function (config) {

            config.headers = config.headers || {};

            var authData = $cookieStore.get('authorizationData');
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }

            return config;
        }

        var _responseError = function (rejection) {
            if (rejection.status === 401) {
                $location.path('/login');
            }
            return $q.reject(rejection);
        }

        authInterceptorServiceFactory.request = _request;
        authInterceptorServiceFactory.responseError = _responseError;

        return authInterceptorServiceFactory;
    }

})(angular.module('common.core'));