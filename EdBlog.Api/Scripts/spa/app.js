﻿(function () {
    'use strict';
    var app = angular.module('mainApp', ['ngRoute', 'common.core', 'common.ui', 'chieffancypants.loadingBar'])
        .config(function ($routeProvider, cfpLoadingBarProvider) {
            $routeProvider
                .when("/",
                {
                    templateUrl: "scripts/spa/layouts/index.html",
                    controller: "indexCtrl"
                })
                .when("/register", {
                    templateUrl: "scripts/spa/layouts/account/signup.html",
                    controller: "signupCtrl"
                })
                .when("/login", {
                    templateUrl: "scripts/spa/layouts/account/login.html",
                    controller: "loginCtrl"
                })
                .when("/post",
                {
                    templateUrl: "scripts/spa/layouts/postDetails.html",
                    controller: "postDetailsCtrl"
                })
                .when("/404",
                {
                    templateUrl: 'scripts/spa/layouts/_shared/_404.html'
                })
                .otherwise({
                    redirectTo: "/404"
                });
            cfpLoadingBarProvider.includeSpinner = true;
        });
    app.run(function run($rootScope, $location, $cookieStore, $http, cfpLoadingBar) {
        // handle page refreshes
        $rootScope.repository = $cookieStore.get('repository') || {};
        if ($rootScope.repository.loggedUser) {
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + $rootScope.repository.loggedUser.authdata;
        }
        $(document).ready(function () {
            $(".fancybox").fancybox({
                openEffect: 'none',
                closeEffect: 'none'
            });

            $('.fancybox-media').fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                helpers: {
                    media: {}
                }
            });

            $('[data-toggle=offcanvas]').click(function () {
                $('.row-offcanvas').toggleClass('active');
            });
        });
        $rootScope.$on('$routeChangeStart', function () {
            cfpLoadingBar.start();
        });

        $rootScope.$on('$routeChangeSuccess', function () {
            cfpLoadingBar.complete();
        });
        $rootScope.$on('$routeChangeError', function () {
            cfpLoadingBar.complete();
        });
    });

})();

