﻿(function (app) {
    'use strict';

    app.directive('widgetRight', widgetRight);

    function widgetRight() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'scripts/spa/layouts/widgetRight.html'
        }
    }

})(angular.module('common.ui'));