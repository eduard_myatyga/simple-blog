﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using EdBlog.Common;
using EdBlog.Common.Interfaces;
using EdBlog.Data;
using EdBlog.Data.Interfaces;
using EdBlog.Data.Repositories;
using EdBlog.Data.UnitOfWork;
using EdBlog.Services.BaseService;

namespace EdBlog.Api.App_Start
{
    public class AutofacWebApiConfig
    {
        public static IContainer Container;
        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }

        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<DbContext>()
                   .As<IEntitiesContext>()
                   .InstancePerRequest();

            builder.RegisterType<UnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerRequest();

            builder.RegisterGeneric(typeof(EntityRepository<>))
                   .As(typeof(IRepository<>))
                   .InstancePerRequest();

            builder.RegisterGeneric(typeof(Service<>))
                    .As(typeof(IService<>))
                    .InstancePerRequest();

            builder.RegisterType<NLogLogger>()
                .As<ILogger>()
                .InstancePerRequest();

            builder.RegisterType<SmtpSettings>()
                .As<ISmtpSettings>()
                .InstancePerRequest();

            builder.RegisterType<ApiConfiguration>()
                .As<IApiConfiguration>()
                .InstancePerRequest();

            Container = builder.Build();

            return Container;
        }
    }
}