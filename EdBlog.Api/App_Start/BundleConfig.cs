﻿using System.Web.Optimization;

namespace EdBlog.Api
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/vendors/jquery.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/vendors/bootstrap.min.js",
                      "~/Scripts/vendors/respond.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/bootstrap.min.css",
                      "~/Content/css/site.css",
                      "~/Content/css/bootstrap-theme.css",
                 "~/Content/css/font-awesome.css",
                "~/Content/css/morris.css",
                "~/Content/css/toastr.css",
                "~/Content/css/jquery.fancybox.css",
                "~/Content/css/loading-bar.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/vendors").Include(
               "~/Scripts/vendors/toastr.js",
               "~/Scripts/vendors/jquery.raty.js",
               "~/Scripts/vendors/respond.src.js",
               "~/Scripts/vendors/angular.js",
               "~/Scripts/vendors/angular-route.js",
               "~/Scripts/vendors/angular-cookies.js",
               "~/Scripts/vendors/angular-validator.js",
               "~/Scripts/vendors/angular-base64.js",
               "~/Scripts/vendors/angular-file-upload.js",
               "~/Scripts/vendors/angucomplete-alt.min.js",
               "~/Scripts/vendors/ui-bootstrap-tpls-0.13.1.js",
               "~/Scripts/vendors/underscore.js",
               "~/Scripts/vendors/raphael.js",
               "~/Scripts/vendors/morris.js",
               "~/Scripts/vendors/jquery.fancybox.js",
               "~/Scripts/vendors/jquery.fancybox-media.js",
               "~/Scripts/vendors/loading-bar.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/spa").Include(
                "~/Scripts/spa/modules/common.core.js",
                "~/Scripts/spa/modules/common.ui.js",
                "~/Scripts/spa/app.js",

                "~/Scripts/spa/services/apiService.js",
               "~/Scripts/spa/services/notificationService.js",
                "~/Scripts/spa/services/membershipService.js",
                "~/Scripts/spa/services/authInterceptorService.js",
               "~/Scripts/spa/directives/sideBar.js",
               "~/Scripts/spa/directives/topBar.js",
               "~/Scripts/spa/directives/widgetRight.js",
               "~/Scripts/spa/controllers/rootCtrl.js",
                "~/Scripts/spa/controllers/indexCtrl.js",
                "~/Scripts/spa/controllers/postDetailsCtrl.js",
                "~/Scripts/spa/controllers/loginCtrl.js",
                 "~/Scripts/spa/controllers/signupCtrl.js"
                
                ));
            BundleTable.EnableOptimizations = false;
        }
    }
}
