﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EdBlog.Common.Helpers
{
    public class Page<T> 
    {
        public int PageIndex { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }
        public int TotalPagesCount { get; private set; }
        public IEnumerable<T> Items { get; set; }

        public bool HasPreviousPage
        {

            get
            {
                return (PageIndex > 1);
            }
        }

        public bool HasNextPage
        {

            get
            {
                return (PageIndex < TotalPagesCount);
            }
        }

        public Page(IEnumerable<T> source, int pageIndex, int pageSize, int totalCount)
        {
            Items = new List<T>();
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            Items = source;
            PageIndex = pageIndex;
            PageSize = pageSize;
            TotalCount = totalCount;
            TotalPagesCount = (int)Math.Ceiling(totalCount / (double)pageSize);
        }
    }
}
