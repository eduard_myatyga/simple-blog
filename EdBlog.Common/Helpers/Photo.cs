﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdBlog.Common.Helpers
{
    public interface IPhoto
    {
        Bitmap GetPhoto();
    }
    public class Photo
    {
        private readonly string _fileName;

        public Photo(string filename)
        {
            this._fileName = filename;
        }
        public Bitmap GetPhoto()
        {
            Bitmap bmp = (Bitmap)Image.FromFile(_fileName);
            return bmp;
        }
    }
    public abstract class DecoratorBase : IPhoto
    {
        private readonly IPhoto _photo;

        protected DecoratorBase(IPhoto photo)
        {
            this._photo = photo;
        }
        public virtual Bitmap GetPhoto()
        {
            return _photo.GetPhoto();
        }
    }

    public class WatermarkDecorator : DecoratorBase
    {
        private string _watermarkText;

        public WatermarkDecorator(IPhoto photo, string watermark)
            : base(photo)
        {
            this._watermarkText = watermark;
        }
        public override Bitmap GetPhoto()
        {
            Bitmap result;
            using (Bitmap bmp = base.GetPhoto())
            {
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    Font font = new Font("Arial", 20, FontStyle.Bold, GraphicsUnit.Pixel);
                    StringFormat sf = new StringFormat
                    {
                        LineAlignment = StringAlignment.Center,
                        Alignment = StringAlignment.Center
                    };
                    float x = (float)bmp.Width / 2;
                    float y = (float)bmp.Height / 2;
                    g.DrawString(_watermarkText, font, Brushes.Black, x, y, sf);
                    g.Save();
                    result = bmp;
                    return result;
                }

            }
        }
    }
}
