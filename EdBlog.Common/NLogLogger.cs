﻿using System;
using EdBlog.Common.Interfaces;
using NLog;
using ILogger = EdBlog.Common.Interfaces.ILogger;

namespace EdBlog.Common
{
    public class NLogLogger : ILogger
    {
        private readonly Logger _logger;
        private readonly ISerializer _serializer;

        public NLogLogger()
        {
            _logger = LogManager.GetCurrentClassLogger();
            _serializer = new Serializer();
        }

        public void Debug(string message, object obj = null)
        {
            SetLogParams(obj);

            _logger.Debug(message);
        }

        public void Info(string message, object obj = null)
        {
            SetLogParams(obj);

            _logger.Info(message);
        }

        public void Warn(string message, object obj = null)
        {
            if (obj is Exception)
            {
                _logger.WarnException(message, obj as Exception);
            }
            else
            {
                SetLogParams(obj);
                _logger.Warn(message);
            }
        }

        public void Error(string message, Exception ex)
        {
            _logger.ErrorException(message, ex);
        }

        private void SetLogParams(object obj)
        {
            string parameter = string.Empty;
            string parameterType = string.Empty;

            if (obj != null)
            {
                parameterType = obj.GetType().FullName;
                parameter = _serializer.SerializeObject(obj);
            }

            GlobalDiagnosticsContext.Set("paramType", parameterType);
            GlobalDiagnosticsContext.Set("param", parameter);
        }
    }
}
