﻿using System.Configuration;
using EdBlog.Common.Interfaces;

namespace EdBlog.Common
{
    public class SmtpSettings : ISmtpSettings
    {
        public string Server
        {
            get { return ConfigurationManager.AppSettings["SmtpServer"]; }
        }

        public int Port
        {
            get { return int.Parse(ConfigurationManager.AppSettings["SmtpPort"]); }
        }

        public string User
        {
            get { return ConfigurationManager.AppSettings["SmtpUser"]; }
        }

        public string Password
        {
            get { return ConfigurationManager.AppSettings["SmtpPassword"]; }
        }

        public string NoReplyEmail
        {
            get { return ConfigurationManager.AppSettings["SmtpNoReplyEmail"]; }
        }

        public string NoReplyName
        {
            get { return ConfigurationManager.AppSettings["SmtpNoReplyName"]; }
        }
    }
}
