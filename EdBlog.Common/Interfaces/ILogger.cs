﻿using System;

namespace EdBlog.Common.Interfaces
{
    public interface ILogger
    {
        void Debug(string message, object obj = null);

        void Info(string message, object obj = null);

        void Warn(string message, object obj = null);

        void Error(string message, Exception ex);
    }
}
