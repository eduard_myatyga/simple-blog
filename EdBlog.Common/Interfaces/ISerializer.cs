﻿namespace EdBlog.Common.Interfaces
{
    public interface ISerializer
    {
        string SerializeObject(object obj);

        T DeserializeObject<T>(string value);
    }
}
