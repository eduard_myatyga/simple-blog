﻿namespace EdBlog.Common.Interfaces
{
    public interface IApiConfiguration
    {
        ILogger Logger { get; }

        ISmtpSettings Smtp { get; }

        string BaseUrl { get; }

        string GetUserDir(int id);

        string BaseUploadDirectory { get; }
    }
}
