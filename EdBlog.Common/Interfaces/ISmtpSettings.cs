﻿namespace EdBlog.Common.Interfaces
{
    public interface ISmtpSettings
    {
        string Server { get; }

        int Port { get; }

        string User { get; }

        string Password { get; }

        string NoReplyEmail { get; }

        string NoReplyName { get; }
    }
}
