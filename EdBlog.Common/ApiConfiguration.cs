﻿using System;
using System.Configuration;
using System.IO;
using EdBlog.Common.Interfaces;

namespace EdBlog.Common
{
    public class ApiConfiguration : IApiConfiguration
    {
        private readonly ILogger _logger;
        private readonly ISmtpSettings _smtp;

        public ApiConfiguration(ILogger logger, ISmtpSettings smtp)
        {
            _logger = logger;
            _smtp = smtp;
        }

        public ILogger Logger
        {
            get { return _logger; }
        }

        public ISmtpSettings Smtp
        {
            get { return _smtp; }
        }

        public string BaseUrl
        {
            get { return ConfigurationManager.AppSettings["BaseUrl"]; }
        }

        public string GetUserDir(int id)
        {
            return GetUserDir(id, null);
        }

        public string GetUserDir(int id, string fileName)
        {
            string root = BaseUploadDirectory;
            string userDir = String.Format("{0}\\users\\user\\{1}", root, id);
            if (!Directory.Exists(userDir))
            {
                Directory.CreateDirectory(userDir);
            }

            return String.IsNullOrWhiteSpace(fileName) ? userDir : String.Format("{0}\\{1}", userDir, fileName);
        }

        public string BaseUploadDirectory
        {
            get { return ConfigurationManager.AppSettings["BaseUploadDirectory"]; }
        }

    }
}
