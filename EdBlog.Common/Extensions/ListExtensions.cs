﻿using System.Collections.Generic;
using System.ComponentModel;
using EdBlog.Common.Helpers;

namespace EdBlog.Common.Extensions
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public static class ListExtensions
    {
        public static Page<T> ToPage<T>(this IEnumerable<T> list, int pageIndex, int pageSize, int total)
        {
            return new Page<T>(list, pageIndex, pageSize, total);
        }
    }
}
