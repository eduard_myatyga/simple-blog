﻿using System.ComponentModel;
using System.Linq;
using EdBlog.Common.Helpers;

namespace EdBlog.Common.Extensions
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public static class QueryableExtensions
    {
        public static Page<T> ToPaginatedList<T>(this IQueryable<T> query, int pageIndex, int pageSize, int total)
        {
            var list = query.ToList();
            return new Page<T>(list, pageIndex, pageSize, total);
        }

        public static IQueryable<T> Paginate<T>(this IQueryable<T> query, int pageIndex, int pageSize)
        {
            var skip = (pageIndex - 1) * pageSize;
            var entities = query.Skip(skip).Take(pageSize);
            return entities;
        }
    }
}
