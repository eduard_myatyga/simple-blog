﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdBlog.Models.Models;

namespace EdBlog.Models.DTOs
{
    public class PostDto
    {
        public string Title { get; private set; }

        public string Meta { get; private set; }

        public string ShortDescription { get; private set; }

        public string Description { get; private set; }

        public string UrlSlug { get; private set; }

        public DateTime PostedOn { get; private set; }

        public DateTime? Modified { get; private set; }

        public string Category { get; private set; }

        public List<string> Tags { get; private set; }

        public string Author { get; private set; }

        public int Likes { get; private set; }

        public PostDto()
        {
            Tags = new List<string>();
        }
        public PostDto(Post post, bool fullDescription = false):this()
        {
            if(post==null)
                throw new ArgumentNullException("post");
            Title = post.Title;
            ShortDescription = fullDescription? null : post.ShortDescription;
            Description = fullDescription ? post.Description : null;
            Meta = post.Meta;
            UrlSlug = post.UrlSlug;
            PostedOn = post.PostedOn;
            Modified = post.Modified;
            Category = post.Category.Name;
            Tags = post.Tags.Select(x => x.Name).ToList();
            Author = post.Author.FirstName;
            Likes = post.Likes.Count;
        }
    }
}
