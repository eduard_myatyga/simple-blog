﻿namespace EdBlog.Models.Enums
{
    public enum ImageType
    {
        PostImage = 1,
        ProfileImage = 2
    }
}
