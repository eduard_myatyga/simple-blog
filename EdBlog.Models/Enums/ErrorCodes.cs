﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdBlog.Models.Enums
{
    public enum ErrorCodes
    {
        UnhandledException = 0,
        ErrorUsernameExist = 1024,
        ErrorNicknameExist = 1025,
        ErrorBadCredentials = 1026,
        ErrorUserNotFound = 1027,
        NoContentFound = 1028,
        UserDoesNotHavePermissions = 1028,
        AccountSuspended = 1029
    }
}
