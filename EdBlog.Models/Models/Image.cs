﻿using System;
using System.Collections.Generic;
using EdBlog.Models.Enums;

namespace EdBlog.Models.Models
{
    public sealed class Image : BaseEntity
    {
        public Guid HasheGuid { get; set; }

        public ImageType Type { get; set; }

        public string Name { get; set; }

        public string PathUrl { get; set; }

        public IList<Post> Posts { get; set; }

        public IList<UserProfile> Users { get; set; } 
    }
}
