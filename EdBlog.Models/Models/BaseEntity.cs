﻿using System.ComponentModel.DataAnnotations;

namespace EdBlog.Models.Models
{
    public class BaseEntity
    {
        [Key]
        public virtual int Id { get; set; }
    }
}
