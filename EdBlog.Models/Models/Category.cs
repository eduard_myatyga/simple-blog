﻿using System.Collections.Generic;

namespace EdBlog.Models.Models
{
    public class Category : BaseEntity
    {
        public string Name { get; set; }

        public string UrlSlug { get; set; }

        public string Description { get; set; }

        public virtual IList<Post> Posts { get; set; }
    }
}
