﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EdBlog.Models.Models.Identity
{
    public class AppUser : IdentityUser
    {
        public virtual UserProfile UserProfile { get; set; }
    }
}
