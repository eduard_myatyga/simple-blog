﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EdBlog.Models.Models.Identity;

namespace EdBlog.Models.Models
{
    public class PostLike : BaseEntity
    {
        public int PostId { get; set; }

        public virtual Post Post { get; set; }

        public string UserProfileId { get; set; }

        public UserProfile UserProfile { get; set; }

        public bool? IsLiked { get; set; } 
    }
}
