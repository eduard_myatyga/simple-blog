﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdBlog.Models.Models.Exceptions
{
    public class ErrorDetails
    {
        [DefaultValue("EMPTY")]
        public string ErrorMessage { get; set; }

        [DefaultValue(0)]
        public int ErrorCode { set; get; }
    }
}
