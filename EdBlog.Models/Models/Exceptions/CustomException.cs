﻿using EdBlog.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdBlog.Models.Models.Exceptions
{
    public class CustomException : Exception
    {
        protected ErrorCodes Code = ErrorCodes.UnhandledException;

        public ErrorCodes GetCode() { return Code; }

        public CustomException(string message)
            : base(message)
        {
        }

        public CustomException(ErrorCodes code, string message)
            : base(message)
        {
            Code = code;
        }
    }
}
