﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EdBlog.Models.Models.Identity;

namespace EdBlog.Models.Models
{
    public class Comment : BaseEntity
    {
        public string Content { get; set; }

        public DateTime PostedOn { get; set; }

        public DateTime? Modified { get; set; }

        public int PostId { get; set; }
        public virtual Post Post { get; set; }

        public string AuthorId { get; set; }
        public virtual UserProfile Author { get; set; }

        public int? ParentCommentId { get; set; }
        public virtual Comment ParentComment { get; set; }
    }
}
