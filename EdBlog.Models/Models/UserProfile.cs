﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EdBlog.Models.Models.Identity;

namespace EdBlog.Models.Models
{
    public class UserProfile : BaseEntity
    {
        [Key]
        public new string Id { get; set; }

        public string NickName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string Address { get; set; }

        public virtual AppUser AppUser { get; set; }

        public int? ImageProfileId { get; set; }

        public Image ImageProfile { get; set; }

        public virtual ICollection<Post> Posts { get; set; }

        public virtual ICollection<PostLike> PostLikes { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
    }
}
