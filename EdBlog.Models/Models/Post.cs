﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EdBlog.Models.Models.Identity;

namespace EdBlog.Models.Models
{
    public class Post : BaseEntity
    {
        public Post()
        {
            Tags = new List<Tag>();
            Comments = new List<Comment>();
            Likes = new List<PostLike>();
            PostImages = new List<Image>();
        }

        public string Title { get; set; }

        public string ShortDescription { get; set; }

        public string Description { get; set; }

        public string Meta { get; set; }

        public string UrlSlug { get; set; }

        public bool Published { get; set; }

        public DateTime PostedOn { get; set; }

        public DateTime? Modified { get; set; }

        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<PostLike> Likes { get; set; }

        public virtual ICollection<Image> PostImages { get; set; }

        public string AuthorId { get; set; }

        public UserProfile Author { get; set; }

    }
}
