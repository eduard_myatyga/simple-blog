﻿using System.Collections.Generic;
using System.Linq;
using EdBlog.Common.Helpers;
using EdBlog.Data.Enum;
using EdBlog.Data.Interfaces;
using EdBlog.Models.DTOs;
using EdBlog.Models.Models;

namespace EdBlog.Services.ServiceExtensions
{
    public static class PostServiceExtinsion 
    {
        public static IEnumerable<Post> GetPost(this IService<Post> service)
        {
            return service.UnitOfWork.Repository<Post>().GetAll();
        }
    }
}
