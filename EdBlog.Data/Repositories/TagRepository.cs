﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdBlog.Data.Interfaces;
using EdBlog.Models.Models;
using EdBlog.Data.Interfaces.Repositories;

namespace EdBlog.Data.Repositories
{
    public class TagRepository : EntityRepository<Tag>, ITagRepository
    {
        public TagRepository(IEntitiesContext context) : base(context)
        {
        }

        public IEnumerable<Tag> Test()
        {
            return DbContext.Database.SqlQuery<Tag>("SELECT *  FROM [EdBlog].[dbo].[Tags] where Name like 'test3'").AsEnumerable();
        }
    }
}
