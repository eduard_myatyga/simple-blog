﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using EdBlog.Data.Identity;
using EdBlog.Models.Models;
using EdBlog.Models.Models.Identity;
using Microsoft.AspNet.Identity;

namespace EdBlog.Data.DataSamples
{
    public class ApplicationDbInitializer : CreateDatabaseIfNotExists<DbContext>
    {
        protected override void Seed(DbContext context)
        {
            InitializeIdentityForEf(context);
            base.Seed(context);
        }

        //Create User=Admin@Admin.com with password=Admin@123456 in the Admin role        
        public void InitializeIdentityForEf(DbContext db)
        {
            try
            {
                // This is only for testing purpose
                const string name = "ed93@live.ru";
                const string nickName = "wazzup";
                const string password = "12345678";
                const string roleName = "Admin";
                var applicationRoleManager = IdentityFactory.CreateRoleManager(db);
                var applicationUserManager = IdentityFactory.CreateUserManager(db);
                //Create Role Admin if it does not exist
                var role = applicationRoleManager.FindByName(roleName);
                if (role == null)
                {
                    role = new ApplicationRole { Name = roleName };
                    applicationRoleManager.Create(role);
                }

                var user = applicationUserManager.FindByName(name);
                if (user == null)
                {
                    user = new AppUser
                    {
                        UserName = name,
                        Email = name,
                        UserProfile =
                            new UserProfile()
                            {
                                Address = "Kharkov",
                                FirstName = "Ed myatyga",
                                NickName = nickName
                            }
                    };
                    applicationUserManager.Create(user, password);
                    applicationUserManager.SetLockoutEnabled(user.Id, false);
                    db.SaveChanges();
                }

                // Add user admin to Role Admin if not already added
                var rolesForUser = applicationUserManager.GetRoles(user.Id);
                if (!rolesForUser.Contains(role.Name))
                {
                    applicationUserManager.AddToRole(user.Id, role.Name);
                }
                db.SaveChanges();

                var post = new Post()
                {
                    AuthorId = user.Id,
                    Author = user.UserProfile,
                    Title = "Test post",
                    Category = new Category()
                    {
                        Description = ".Net",
                        Name = ".Net",
                        UrlSlug = "dotNet"
                    },
                    Description =
                        @"Aries is the first sign of the zodiac, and that's pretty much how those born under this sign see themselves: first. Aries are the leaders of the pack, first in line to get things going. Whether or not everything gets done is another question altogether, for an Aries prefers to initiate rather than to complete. Do you have a project needing a kick-start? Call an Aries, by all means. The leadership displayed by Aries is most impressive, so don't be surprised if they can rally the troops against seemingly insurmountable odds.",
                    ShortDescription =
                        @"Aries is the first sign of the zodiac, and that's pretty much how those born under this sign see themselves: first. Aries are the leaders of the pack, first in line to get things going. ",
                    Meta = ".net;web",
                    UrlSlug = "Test_post",
                    Published = true,
                    PostedOn = DateTime.UtcNow,
                };
                db.Posts.Add(post);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                // ignored
            }
        }

    }
}
