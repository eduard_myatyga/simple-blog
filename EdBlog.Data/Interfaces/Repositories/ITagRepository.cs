﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdBlog.Models.Models;

namespace EdBlog.Data.Interfaces.Repositories
{
    public interface ITagRepository: IRepository<Tag>
    {
        IEnumerable<Tag> Test();
    }
}
