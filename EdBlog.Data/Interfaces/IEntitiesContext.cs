﻿using System;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using EdBlog.Common.Interfaces;
using EdBlog.Models.Models;

namespace EdBlog.Data.Interfaces
{
    public interface IEntitiesContext : IDisposable
    {
        System.Data.Entity.DbContext GetContext { get; }
        IApiConfiguration Configuration { get; }
        IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity;
        void SetAsAdded<TEntity>(TEntity entity) where TEntity : BaseEntity;
        void SetAsModified<TEntity>(TEntity entity) where TEntity : BaseEntity;
        void SetAsDeleted<TEntity>(TEntity entity) where TEntity : BaseEntity;
        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        void BeginTransaction();
        int Commit();
        void Rollback();
        Task<int> CommitAsync();
    }
}
