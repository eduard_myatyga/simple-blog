﻿using System;
using System.Threading;
using System.Threading.Tasks;
using EdBlog.Data.Interfaces.Repositories;
using EdBlog.Models.Models;

namespace EdBlog.Data.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        int SaveChanges();
        void Dispose(bool disposing);
        ITagRepository TagRepository { get; }
        IRepository<TEntity> Repository<TEntity>() where TEntity : BaseEntity;
        void BeginTransaction();
        int Commit();
        void Rollback();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        Task<int> CommitAsync();
    }
}
