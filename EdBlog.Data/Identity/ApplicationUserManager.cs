﻿using EdBlog.Data.Interfaces.Identity;
using EdBlog.Models.Models.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace EdBlog.Data.Identity
{
    public class ApplicationUserManager : UserManager<AppUser>, IApplicationUserManager
    {
        public ApplicationUserManager(IUserStore<AppUser> store)
            : base(store)
        {
        }
        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            
            return IdentityFactory.CreUserManager(options,context);
        }
    }
}
