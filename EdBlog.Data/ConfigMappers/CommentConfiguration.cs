﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdBlog.Models.Models;

namespace EdBlog.Data.ConfigMappers
{
    public class CommentConfiguration : EntityBaseConfiguration<Comment>
    {
        public CommentConfiguration()
        {
            ToTable("T_Comments");
            Property(x => x.Content).IsRequired().HasMaxLength(10000);
            Property(x => x.PostId).HasColumnName("PostId");
            Property(x => x.AuthorId).HasColumnName("AuthorId");
            Property(x => x.PostedOn).IsRequired();
            HasRequired(x => x.Post).WithMany(x => x.Comments).HasForeignKey(x => x.PostId).WillCascadeOnDelete(false);
            HasRequired(x => x.Author).WithMany(x => x.Comments).HasForeignKey(x => x.AuthorId).WillCascadeOnDelete(false);
            HasOptional(x => x.ParentComment).WithMany().HasForeignKey(x => x.ParentCommentId);
        }
    }
}
