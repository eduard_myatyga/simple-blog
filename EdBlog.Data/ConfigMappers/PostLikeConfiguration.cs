﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdBlog.Models.Models;

namespace EdBlog.Data.ConfigMappers
{
    public class PostLikeConfiguration : EntityBaseConfiguration<PostLike>
    {
        public PostLikeConfiguration()
        {
            ToTable("T_PostLikes");
            Property(x => x.PostId).HasColumnName("PostId");
            Property(x => x.UserProfileId).HasColumnName("UserProfileId");
            HasRequired(b => b.Post).WithMany(i => i.Likes).HasForeignKey(b => b.PostId);
            HasRequired(x => x.UserProfile).WithMany(i => i.PostLikes).HasForeignKey(x => x.UserProfileId).WillCascadeOnDelete(false);
        }
    }
}
