﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdBlog.Models.Models;

namespace EdBlog.Data.ConfigMappers
{
    public class TagConfiguration : EntityBaseConfiguration<Tag>
    {
        public TagConfiguration()
        {
            ToTable("T_Tags");
            Property(x => x.Name).IsRequired();
            Property(x => x.Description).IsOptional();
            Property(x => x.UrlSlug).IsRequired();
        }
    }
}
