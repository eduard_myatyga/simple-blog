﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdBlog.Models.Models;

namespace EdBlog.Data.ConfigMappers
{
    public class CategoryConfiguration : EntityBaseConfiguration<Category>
    {
        public CategoryConfiguration()
        {
            ToTable("T_Categories");
            Property(x => x.Name).IsRequired().HasMaxLength(50);
            Property(x => x.UrlSlug).HasMaxLength(150).IsRequired();
            Property(x => x.Description).HasMaxLength(300);
        }
    }
}
