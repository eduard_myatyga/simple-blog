﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdBlog.Models.Models;

namespace EdBlog.Data.ConfigMappers
{
    public class PostConfiguration : EntityBaseConfiguration<Post>
    {
        public PostConfiguration()
        {
            ToTable("T_Posts");
            Property(m => m.Title).IsRequired().HasMaxLength(250)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_Title", 1) { IsUnique = true }));
            Property(m => m.Description).IsRequired().IsMaxLength();
            Property(m => m.Modified).IsOptional();
            Property(m => m.ShortDescription).IsRequired();
            Property(m => m.PostedOn).IsRequired();
            Property(m => m.UrlSlug).IsRequired().HasMaxLength(300);
            Property(m => m.AuthorId).IsRequired();
            Property(m => m.Meta).IsOptional().HasMaxLength(300);
            HasRequired(x => x.Category).WithMany(x => x.Posts).HasForeignKey(x=>x.CategoryId);
            HasRequired(x => x.Author).WithMany(x => x.Posts).HasForeignKey(x=>x.AuthorId);
            HasMany(i => i.Tags).WithMany(c => c.Posts)
            .Map(mc =>
            {
                mc.MapLeftKey("TagId");
                mc.MapRightKey("PostId");
                mc.ToTable("T_PostTags");
            });
            HasMany(c => c.PostImages).WithMany(x => x.Posts).Map(mc =>
            {
                mc.MapLeftKey("ImageId");
                mc.MapRightKey("PostId");
                mc.ToTable("T_PostImages");
            });
        }
    }
}
