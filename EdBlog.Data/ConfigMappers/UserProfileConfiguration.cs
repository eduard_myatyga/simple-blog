﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdBlog.Models.Models;

namespace EdBlog.Data.ConfigMappers
{
    public class UserProfileConfiguration: EntityBaseConfiguration<UserProfile>
    {
        public UserProfileConfiguration()
        {
            ToTable("T_UserProfiles");
            Ignore(x => x.FullName);
            Property(x => x.FirstName).IsOptional();
            Property(x => x.LastName).IsOptional();
            Property(x => x.Address).IsOptional();
            HasOptional(x => x.ImageProfile).WithMany(x => x.Users).HasForeignKey(x => x.ImageProfileId);
            Property(x => x.NickName).IsRequired().HasMaxLength(100)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_NickName", 1) { IsUnique = true }));
        }
    }
}
