﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdBlog.Models.Models;

namespace EdBlog.Data.ConfigMappers
{
    public class ImageConfiguration : EntityBaseConfiguration<Image>
    {
        public ImageConfiguration()
        {
            ToTable("T_Images");
            Property(t => t.Type).IsRequired();
            Property(t => t.PathUrl).IsOptional().HasMaxLength(1000);
            Property(t => t.Name).IsOptional();
        }
    }
}
