﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdBlog.Models.Models;

namespace EdBlog.Data.ConfigMappers
{
    public class EntityBaseConfiguration<T> : EntityTypeConfiguration<T> where T : BaseEntity
    {
        public EntityBaseConfiguration()
        {
            HasKey(e => e.Id);
        }
    }
}
