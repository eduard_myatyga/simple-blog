namespace EdBlog.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.T_Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        UrlSlug = c.String(nullable: false, maxLength: 150),
                        Description = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T_Posts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 250),
                        ShortDescription = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        Meta = c.String(maxLength: 300),
                        UrlSlug = c.String(nullable: false, maxLength: 300),
                        Published = c.Boolean(nullable: false),
                        PostedOn = c.DateTime(nullable: false),
                        Modified = c.DateTime(),
                        CategoryId = c.Int(nullable: false),
                        AuthorId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.T_UserProfiles", t => t.AuthorId, cascadeDelete: true)
                .ForeignKey("dbo.T_Categories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.Title, unique: true)
                .Index(t => t.CategoryId)
                .Index(t => t.AuthorId);
            
            CreateTable(
                "dbo.T_UserProfiles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        NickName = c.String(nullable: false, maxLength: 100),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Address = c.String(),
                        ImageProfileId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.T_Accounts", t => t.Id, cascadeDelete: true)
                .ForeignKey("dbo.T_Images", t => t.ImageProfileId)
                .Index(t => t.Id)
                .Index(t => t.NickName, unique: true)
                .Index(t => t.ImageProfileId);
            
            CreateTable(
                "dbo.T_Accounts",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.UserId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.T_UserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        IdentityUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.T_Accounts", t => t.IdentityUser_Id)
                .Index(t => t.IdentityUser_Id);
            
            CreateTable(
                "dbo.T_UserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                        IdentityUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.T_Accounts", t => t.IdentityUser_Id)
                .Index(t => t.IdentityUser_Id);
            
            CreateTable(
                "dbo.T_UserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                        IdentityUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.T_Roles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.T_Accounts", t => t.IdentityUser_Id)
                .Index(t => t.RoleId)
                .Index(t => t.IdentityUser_Id);
            
            CreateTable(
                "dbo.T_Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false),
                        PostedOn = c.DateTime(nullable: false),
                        Modified = c.DateTime(),
                        PostId = c.Int(nullable: false),
                        AuthorId = c.String(nullable: false, maxLength: 128),
                        ParentCommentId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.T_UserProfiles", t => t.AuthorId)
                .ForeignKey("dbo.T_Comments", t => t.ParentCommentId)
                .ForeignKey("dbo.T_Posts", t => t.PostId)
                .Index(t => t.PostId)
                .Index(t => t.AuthorId)
                .Index(t => t.ParentCommentId);
            
            CreateTable(
                "dbo.T_Images",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HasheGuid = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Name = c.String(),
                        PathUrl = c.String(maxLength: 1000),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T_PostLikes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PostId = c.Int(nullable: false),
                        UserProfileId = c.String(nullable: false, maxLength: 128),
                        IsLiked = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.T_Posts", t => t.PostId, cascadeDelete: true)
                .ForeignKey("dbo.T_UserProfiles", t => t.UserProfileId)
                .Index(t => t.PostId)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.T_Tags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        UrlSlug = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.T_Roles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.T_PostImages",
                c => new
                    {
                        ImageId = c.Int(nullable: false),
                        PostId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ImageId, t.PostId })
                .ForeignKey("dbo.T_Posts", t => t.ImageId, cascadeDelete: true)
                .ForeignKey("dbo.T_Images", t => t.PostId, cascadeDelete: true)
                .Index(t => t.ImageId)
                .Index(t => t.PostId);
            
            CreateTable(
                "dbo.T_PostTags",
                c => new
                    {
                        TagId = c.Int(nullable: false),
                        PostId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TagId, t.PostId })
                .ForeignKey("dbo.T_Posts", t => t.TagId, cascadeDelete: true)
                .ForeignKey("dbo.T_Tags", t => t.PostId, cascadeDelete: true)
                .Index(t => t.TagId)
                .Index(t => t.PostId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.T_UserRoles", "IdentityUser_Id", "dbo.T_Accounts");
            DropForeignKey("dbo.T_UserLogins", "IdentityUser_Id", "dbo.T_Accounts");
            DropForeignKey("dbo.T_UserClaims", "IdentityUser_Id", "dbo.T_Accounts");
            DropForeignKey("dbo.T_UserRoles", "RoleId", "dbo.T_Roles");
            DropForeignKey("dbo.T_PostTags", "PostId", "dbo.T_Tags");
            DropForeignKey("dbo.T_PostTags", "TagId", "dbo.T_Posts");
            DropForeignKey("dbo.T_PostImages", "PostId", "dbo.T_Images");
            DropForeignKey("dbo.T_PostImages", "ImageId", "dbo.T_Posts");
            DropForeignKey("dbo.T_Posts", "CategoryId", "dbo.T_Categories");
            DropForeignKey("dbo.T_Posts", "AuthorId", "dbo.T_UserProfiles");
            DropForeignKey("dbo.T_PostLikes", "UserProfileId", "dbo.T_UserProfiles");
            DropForeignKey("dbo.T_PostLikes", "PostId", "dbo.T_Posts");
            DropForeignKey("dbo.T_UserProfiles", "ImageProfileId", "dbo.T_Images");
            DropForeignKey("dbo.T_Comments", "PostId", "dbo.T_Posts");
            DropForeignKey("dbo.T_Comments", "ParentCommentId", "dbo.T_Comments");
            DropForeignKey("dbo.T_Comments", "AuthorId", "dbo.T_UserProfiles");
            DropForeignKey("dbo.T_UserProfiles", "Id", "dbo.T_Accounts");
            DropIndex("dbo.T_PostTags", new[] { "PostId" });
            DropIndex("dbo.T_PostTags", new[] { "TagId" });
            DropIndex("dbo.T_PostImages", new[] { "PostId" });
            DropIndex("dbo.T_PostImages", new[] { "ImageId" });
            DropIndex("dbo.T_Roles", "RoleNameIndex");
            DropIndex("dbo.T_PostLikes", new[] { "UserProfileId" });
            DropIndex("dbo.T_PostLikes", new[] { "PostId" });
            DropIndex("dbo.T_Comments", new[] { "ParentCommentId" });
            DropIndex("dbo.T_Comments", new[] { "AuthorId" });
            DropIndex("dbo.T_Comments", new[] { "PostId" });
            DropIndex("dbo.T_UserRoles", new[] { "IdentityUser_Id" });
            DropIndex("dbo.T_UserRoles", new[] { "RoleId" });
            DropIndex("dbo.T_UserLogins", new[] { "IdentityUser_Id" });
            DropIndex("dbo.T_UserClaims", new[] { "IdentityUser_Id" });
            DropIndex("dbo.T_Accounts", "UserNameIndex");
            DropIndex("dbo.T_UserProfiles", new[] { "ImageProfileId" });
            DropIndex("dbo.T_UserProfiles", new[] { "NickName" });
            DropIndex("dbo.T_UserProfiles", new[] { "Id" });
            DropIndex("dbo.T_Posts", new[] { "AuthorId" });
            DropIndex("dbo.T_Posts", new[] { "CategoryId" });
            DropIndex("dbo.T_Posts", new[] { "Title" });
            DropTable("dbo.T_PostTags");
            DropTable("dbo.T_PostImages");
            DropTable("dbo.T_Roles");
            DropTable("dbo.T_Tags");
            DropTable("dbo.T_PostLikes");
            DropTable("dbo.T_Images");
            DropTable("dbo.T_Comments");
            DropTable("dbo.T_UserRoles");
            DropTable("dbo.T_UserLogins");
            DropTable("dbo.T_UserClaims");
            DropTable("dbo.T_Accounts");
            DropTable("dbo.T_UserProfiles");
            DropTable("dbo.T_Posts");
            DropTable("dbo.T_Categories");
        }
    }
}
