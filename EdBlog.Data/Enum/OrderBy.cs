﻿namespace EdBlog.Data.Enum
{
    public enum OrderBy
    {
        Ascending,
        Descending
    }
}
