﻿using System.Data.Entity;
using EdBlog.Data.ConfigMappers;
using EdBlog.Models.Models;
using EdBlog.Models.Models.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EdBlog.Data
{
    public class EfConfig
    {
        public static void ConfigureEf(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserProfile>().HasKey(x => x.Id);
            modelBuilder.Entity<AppUser>().HasOptional(t => t.UserProfile).WithRequired(t => t.AppUser).WillCascadeOnDelete(true);
            modelBuilder.Configurations.Add(new PostConfiguration());
            modelBuilder.Configurations.Add(new CommentConfiguration());
            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new ImageConfiguration());
            modelBuilder.Configurations.Add(new PostLikeConfiguration());
            modelBuilder.Configurations.Add(new TagConfiguration());
            modelBuilder.Configurations.Add(new UserProfileConfiguration());
        }
    }
}
