﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using EdBlog.Common;
using EdBlog.Common.Interfaces;
using EdBlog.Data.DataSamples;
using EdBlog.Data.Interfaces;
using EdBlog.Models.Models;
using EdBlog.Models.Models.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EdBlog.Data
{
    public class DbContext : IdentityDbContext<AppUser>, IEntitiesContext
    {
        private ObjectContext _objectContext;
        private DbTransaction _transaction;
        private static readonly object Lock = new object();
        private static bool _databaseInitialized;

        private readonly IApiConfiguration _configuration;

        public ILogger Logger
        {
            get { return _configuration.Logger; }
        }


        public DbSet<Post> Posts { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<PostLike> Likes { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<UserProfile> Profiles { get; set; }

        //        static DbContext()
        //        {
        //            Database.SetInitializer(new ApplicationDbInitializer());
        //        }
        public DbContext(IApiConfiguration configuration)
            : base("Name=DbConnection")
        {
            //Database.Log = logger.Log;
            _configuration = configuration;
            base.Configuration.ProxyCreationEnabled = false;
           
            if (_databaseInitialized)
            {
                return;
            }
            lock (Lock)
            {
                if (!_databaseInitialized)
                {
                    // Set the database intializer which is run once during application start
                    // This seeds the database with admin user credentials and admin role

                    // Database.SetInitializer(new ApplicationDbInitializer());
                    Database.SetInitializer(new ApplicationDbInitializer());
                    _databaseInitialized = true;
                }
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            EfConfig.ConfigureEf(modelBuilder);
            //http://stackoverflow.com/questions/19460386/how-can-i-change-the-table-names-when-using-visual-studio-2013-aspnet-identity
            // resolved problem How can I change the table names when using Visual Studio 2013 AspNet Identity?
            modelBuilder.Entity<IdentityUser>().ToTable("T_Accounts").Property(p => p.Id).HasColumnName("UserId");
            modelBuilder.Entity<AppUser>().ToTable("T_Accounts").Property(p => p.Id).HasColumnName("UserId");
            modelBuilder.Entity<IdentityUserRole>().ToTable("T_UserRoles");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("T_UserLogins");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("T_UserClaims");
            modelBuilder.Entity<IdentityRole>().ToTable("T_Roles");
        }

        public System.Data.Entity.DbContext GetContext
        {
            get { return this; }
        }

        public new IApiConfiguration Configuration
        {
            get { return _configuration; }
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        public void SetAsAdded<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            UpdateEntityState(entity, EntityState.Added);
        }

        public void SetAsModified<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            UpdateEntityState(entity, EntityState.Modified);
        }

        public void SetAsDeleted<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            UpdateEntityState(entity, EntityState.Deleted);
        }

        public void BeginTransaction()
        {
            _objectContext = ((IObjectContextAdapter)this).ObjectContext;
            if (_objectContext.Connection.State == ConnectionState.Open)
            {
                return;
            }
            _objectContext.Connection.Open();
            _transaction = _objectContext.Connection.BeginTransaction();
        }

        public int Commit()
        {
            try
            {
                BeginTransaction();
                var saveChanges = SaveChanges();
                _transaction.Commit();

                return saveChanges;
            }
            catch (Exception)
            {
                Rollback();
                throw;
            }
            finally
            {
                Dispose();
            }
        }

        public void Rollback()
        {
            _transaction.Rollback();
        }

        public async Task<int> CommitAsync()
        {
            try
            {
                BeginTransaction();
                var saveChangesAsync = await SaveChangesAsync();
                _transaction.Commit();

                return saveChangesAsync;
            }
            catch (Exception)
            {
                Rollback();
                throw;
            }
            finally
            {
                Dispose();
            }
        }

        private void UpdateEntityState<TEntity>(TEntity entity, EntityState entityState) where TEntity : BaseEntity
        {
            var dbEntityEntry = GetDbEntityEntrySafely(entity);
            dbEntityEntry.State = entityState;
        }

        private DbEntityEntry GetDbEntityEntrySafely<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            var dbEntityEntry = Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                Set<TEntity>().Attach(entity);
            }
            return dbEntityEntry;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_objectContext != null && _objectContext.Connection.State == ConnectionState.Open)
                {
                    _objectContext.Connection.Close();
                }
                if (_objectContext != null)
                {
                    _objectContext.Dispose();
                }
                if (_transaction != null)
                {
                    _transaction.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        public static DbContext Create()
        {
            return new DbContext(new ApiConfiguration(new NLogLogger(), new SmtpSettings()));
        }
    }
}
